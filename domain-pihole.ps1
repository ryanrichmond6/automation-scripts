$InputFile = 'C:\filepath.txt'
$addresses = get-content $InputFile
$reader = New-Object IO.StreamReader $InputFile

$adList = @{}

    while($reader.ReadLine() -ne $null){ $TotalIPs++ }
write-host    ""    
write-Host "Performing nslookup on each address..."    
        foreach($address in $addresses) {
            ## Progress bar
            $i++
            $percentdone = (($i / $TotalIPs) * 100)
            $percentdonerounded = "{0:N0}" -f $percentdone
            Write-Progress -Activity "Performing nslookups" -CurrentOperation "Working on IP: $address (IP $i of $TotalIPs)" -Status "$percentdonerounded% complete" -PercentComplete $percentdone
            ## End progress bar
            try {
                $resolveHost = [system.net.dns]::resolve($address) | Select HostName,AddressList
                }
                catch {
                    Write-host "$address was not found. $_" -ForegroundColor Green
                }
                $adList.add($resolveHost.hostname, $resolveHost.addresslist) 
            }
write-host    ""  
# $adList.GetEnumerator()  | Export-Csv -Path "C:\filepath\test.csv" -notype

#$adList.GetEnumerator() | Select-Object -Property Key,Value | Export-Csv -NoTypeInformation -Path "C:\filepath\test.csv"

# $adList.GetEnumerator() | ConvertTo-Html -Property Key,Value | Out-File "C:\filepath\test.html"

foreach ( $h in $adList.GetEnumerator()) { write-output "$($h.name) : $($h.value)" >> "C:\filepath\test.txt" }

$SourceFile = "C:\filepath\test.txt"
$DestFile = "C:\filepath\test.htm"

$File = Get-Content $SourceFile
<# $FileLine = @()
Foreach ($Line in $File) {
 $MyObject = New-Object -TypeName PSObject
 Add-Member -InputObject $MyObject -Type NoteProperty -Name Adware Domains -Value $Line
 $FileLine += $MyObject
}
$FileLine | ConvertTo-Html -Property HealthCheck -body "&lt;H2&gt;Domains&lt;/H2&gt;" | Out-File $DestFile
#>

Import-Csv $SourceFile -Delimiter ":" | export-csv C:\filepath\test.csv 

<# 
$excel = new-Object -comobject Excel.Application
$excel.visible = $true # set it to $false if you don't need monitoring the actions...
$workBook = $excel.Workbooks.Add()
$sheet =  $workBook.Sheets.Item(1)
$sheet.Name = "Computers List"
$sheet.Range("A1","A2").ColumnWidth = 40
$sheet.range('A:A').VerticalAlignment = -4160 #align is center (TOP -4108 Bottom -4107 Normal)

$sheet.Cells.Item(1,1) = "FQDN"
$sheet.cells.Item(1,2) = "IP Address"

$index = 2

$adList.keys | % {  

    $sheet.Cells.Item($index,1) = $_
    $sheet.Cells.Item($index,2) = $adList.item($_)
    $index++
}

$workBook.SaveAs("C:\filepath\test.xls")
$excel.Quit()
#>
        
<# write-Host "Pinging each address..."
        foreach($address in $addresses) {
            ## Progress bar
            $j++
            $percentdone2 = (($j / $TotalIPs) * 100)
            $percentdonerounded2 = "{0:N0}" -f $percentdone2
            Write-Progress -Activity "Performing pings" -CurrentOperation "Pinging IP: $address (IP $j of $TotalIPs)" -Status "$percentdonerounded2% complete" -PercentComplete $percentdone2
            ## End progress bar
                if (test-Connection -ComputerName $address -Count 2 -Quiet ) {  
                    write-Host "$address responded" -ForegroundColor Green 
                    } else 
                    { Write-Warning "$address does not respond to pings"              
                    }  
        } #>
write-host    ""        
write-host "Done!"